import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";
import 'antd/dist/antd.css'
import {observer} from "mobx-react-lite";
import {
    QueryClient,
    QueryClientProvider,
} from 'react-query'

import './App.css';
import AuthRouter from "./routers/AuthRouter";
import authStore from "./stores/authStore";
import AppRouter from "./routers/AppRouter";

const queryClient = new QueryClient()

function App() {

    return (
        <div className="App">
            <QueryClientProvider client={queryClient}>
                <Router>
                    <Switch>
                        <Route path={['/login', '/register']} exact>
                            {
                                authStore.token
                                    ? <Redirect to="/profile"/>
                                    : <AuthRouter/>
                            }
                        </Route>

                        <Route path={['/profile', '/profile/:boardID', '/search']} exact>
                            {
                                authStore.token
                                    ? <AppRouter/>
                                    : <Redirect to="/login"/>
                            }
                        </Route>

                        <Route path={'/'} exact>
                            <Redirect to="/login"/>
                        </Route>
                    </Switch>
                </Router>
            </QueryClientProvider>
        </div>
    );
}

export default observer(App);
