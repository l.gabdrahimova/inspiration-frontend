import React from "react";
import {render, unmountComponentAtNode} from "react-dom";
import ReactTestUtils, {act} from "react-dom/test-utils";
import {MemoryRouter} from "react-router-dom";
import {QueryClient, QueryClientProvider} from "react-query";
import EditProfileModal from "./EditProfileModal";

let container: Element | null = null;

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);

    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation(query => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // Deprecated
            removeListener: jest.fn(), // Deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });
});

afterEach(() => {
    if (!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


jest.mock("../../stores/authStore", () => ({
    token: '1000'
}));

jest.mock("../../api/api", () => ({
    updateProfile: async () => {
    },

    uploadAvatar: async () => {
    },
}));
jest.mock("antd", () => ({
// @ts-ignore
    ...jest.requireActual("antd"),
    Modal: ({children}) => <div>{children}</div>
}));


const testQueryClient = new QueryClient()

it("Board page without images", async () => {
    if (!container)
        return

    await act(async () => {
        render(
            <EditProfileModal
            visible={true}
            profile={{
                username: 'test',
                first_name: 'Name',
                email: 'test',
                id: 1,
                last_name: 'Test'
            }}/>, container);


        await sleep(100)
    });

    expect(container.getElementsByClassName('modal__title')[0].textContent).toBe("Edit profile");
    ReactTestUtils.Simulate.submit(container.getElementsByTagName('form')[0]);
    // expect(container.getElementsByTagName('h2')[0].textContent).toBe("Name Test");
    //
    // // @ts-ignore
    // expect(container.getElementsByClassName('BoardsGallery__noContent')[0].textContent).toBe("You don't have any collectionsso there's nothing to display here. Sad panda.Add a photo now!");
});