import React, {useEffect, useState} from 'react';
import {Button, Form, Input, message, Modal, notification, Upload} from "antd";
import {UserOutlined, FileImageOutlined} from "@ant-design/icons";
import {RcFile, UploadChangeParam} from "antd/es/upload";
import {UploadFile} from "antd/es/upload/interface";
import {updateProfile, uploadAvatar} from "../../api/api";

type EditProfileModalProps = {
    visible: boolean
    profile?: MeResponse
    onCancel?: () => any
}

const normFile = (e: UploadChangeParam<UploadFile<Blob>>) => {
    console.log('Upload event:',  e.file.originFileObj)

    if (e?.file?.originFileObj!=null) {
        return e.file.originFileObj
    }
};
const toBase64 = (file: Blob) => new Promise<string>((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result?.toString()||'');
    reader.onerror = error => reject(error);
});


function beforeUpload(file: RcFile) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
        message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
}


const EditProfileModal = ({profile, visible, onCancel}: EditProfileModalProps) => {
    const [imageUrl, setImageUrl] = useState('')
    const [form] = Form.useForm()

    useEffect(()=>{setImageUrl('')},[visible])

    const handleChange = async (info: UploadChangeParam<UploadFile<Blob>>) => {
        if (info.file.originFileObj!=null) {
            const url = await toBase64(info.file.originFileObj)
            setImageUrl(url)
        }
    };

    const onFinish = async (values: {first_name: string, last_name: string, avatar?: File}) => {

        if(values.avatar != null){
            try {
                const formData = new FormData();
                formData.append('pic', values.avatar)
                await uploadAvatar(formData)
            }catch (e){
                notification.error({message: e.message})
            }
        }
        if(profile){
            try {
                await updateProfile(profile.id, values)
            }catch (e){
                notification.error({message: e.message})
            }
        }

        onCancel&&onCancel()
    }

    if (!visible || !profile)
        return <></>

    return (
        <Modal
            visible={true}
            title={false}
            footer={false}
            className={'modal'}
            width={380}
            closable={false}
            onCancel={onCancel}
        >

            <div className="modal__title">Edit profile</div>
            <Form initialValues={profile} onFinish={onFinish} form={form}>
                <Form.Item
                    name="first_name"
                    rules={[{required: true, message: 'Please input your First name!'}]}
                >
                    <Input
                        style={{padding: 16}}
                        prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="First name"/>
                </Form.Item>

                <Form.Item
                    name="last_name"
                    rules={[{required: true, message: 'Please input your Last name!'}]}
                >
                    <Input
                        style={{padding: 16}}
                        prefix={<UserOutlined className="site-form-item-icon"/>} placeholder="Last name"/>
                </Form.Item>

                <Form.Item name="avatar" valuePropName="file" getValueFromEvent={normFile}>
                    <Upload.Dragger
                        name="avatar"
                        maxCount={1}
                        beforeUpload={beforeUpload}
                        onChange={handleChange}
                        customRequest={()=>{}}
                        showUploadList={false}

                    >
                        {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : <>

                            <p className="ant-upload-drag-icon">
                                {/*<InboxOutlined />*/}
                                <FileImageOutlined />
                                {/*<UserOutlined />*/}
                            </p>
                            <p className="ant-upload-text">Click or drag your new avatar</p>
                            <p className="ant-upload-hint">JPG or PNG, less then 2MB.</p>
                        </>}




                    </Upload.Dragger>
                </Form.Item>

                <Form.Item noStyle>
                    <Button
                        style={{
                            background: '#27AE60',
                            borderRadius: 10,
                            width: '100%',
                            height: 38,
                            borderColor: `#2bbe68`

                        }}
                        type="primary"
                        htmlType="submit"
                    >
                        Save
                    </Button>
                </Form.Item>
            </Form>


        </Modal>
    )
};

export default EditProfileModal;