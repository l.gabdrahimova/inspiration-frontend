import React, {useState} from 'react';
import './ProfilePage.css';
import {useQuery} from "react-query";
import {deleteBoard, getBoards, getMe} from "../../api/api";
import authStore from "../../stores/authStore";
import Tool from "../../components/Tool";
import BoardsGallery from "../../components/BoardsGallery";
import {useHistory} from 'react-router-dom';
import Loading from "../../components/Loading";
import {notification} from "antd";
import EditProfileModal from "./EditProfileModal";


const ProfilePage = () => {
    const {data: meResponse, refetch: refetchMe, isLoading: profileLoading} = useQuery(`${authStore.token}-profile`, getMe)
    const {data: boardsResponse, isLoading: boardsLoading, refetch: refetchBoards} = useQuery(`${authStore.token}-boards`, getBoards)
    const [isModalVisible, setIsModalVisible] = useState(false);
    const history = useHistory()



    if(profileLoading || boardsLoading)
        return <Loading/>

    return (
        <div className="ProfilePage">
            <div className="ProfilePage__header">
                <div className="ProfilePage__profileInfo">
                    <h2>
                        {meResponse?.data.first_name || 'First name'} {meResponse?.data.last_name || 'Last name'}
                    </h2>
                    <h1>
                        @{meResponse?.data.username || 'loading'}
                    </h1>
                    <div className="ProfilePage__profileInfo_tools">
                        <Tool type={"edit"} size={'large'} onClick={() => setIsModalVisible(true)}/>
                        <EditProfileModal visible={isModalVisible} profile={meResponse?.data} onCancel={async ()=>{
                            await refetchMe()
                            setIsModalVisible(false)
                        }}/>
                    </div>
                </div>

                <div className="ProfilePage__profilePictures">
                    <img src={meResponse?.data.profile?.pic||"/no_avatar.png"} alt={'avatar'}/>
                </div>
            </div>

            <BoardsGallery
                onChoose={(id) => {
                    history.push(`/profile/${id}`)
                    window.scrollTo(0, 0);
                }}
                onRemove={(id)=>{
                    deleteBoard(id)
                        .then(r => refetchBoards())
                        .catch(e => notification.error({message: e.message}))
                }}
                boards={boardsResponse?.data || []}

            />

        </div>
    )
};

export default ProfilePage;