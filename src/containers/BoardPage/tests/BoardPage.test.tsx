import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import ReactTestUtils, { act } from "react-dom/test-utils";
import { MemoryRouter } from "react-router-dom";
import BoardPage from "../BoardPage";
import {QueryClient, QueryClientProvider} from "react-query";
let container: Element | null = null;

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);

    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation(query => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // Deprecated
            removeListener: jest.fn(), // Deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });
});

afterEach(() => {
    if(!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


jest.mock("../../../stores/authStore", () => ({
    token: '1000'
}));

jest.mock("../../../api/api", () => ({
    getBoard: async ()=>{
        return {
            data: {
                id: 10,
                cover: {src: 'https://somelink.com'},
                name: 'Test board',
            }
        }
    },
    getBoardItems: async ()=>{
        return {
            data: []
        }
    },
}));


const testQueryClient = new QueryClient()

it("Board page without images", async () => {
    if(!container)
        return

    await act(async () => {
        render(<QueryClientProvider client={testQueryClient}><MemoryRouter><BoardPage/></MemoryRouter></QueryClientProvider>, container);


        await sleep(100)
    });

    expect(container.getElementsByTagName('h1')[0].textContent).toBe("Test board");

    // @ts-ignore
    expect(container.getElementsByClassName('BoardsGallery__noContent')[0].textContent).toBe("You don't have any photos in this collectionso there's nothing to display here. Sad panda.Add a photo now!");
});
