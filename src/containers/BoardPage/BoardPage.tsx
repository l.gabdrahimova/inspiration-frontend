import React from 'react';

import './BoardPage.css';
import Tool from "../../components/Tool";
import ImagesGallery from "../../components/ImagesGallery";
import {useQuery} from "react-query";
import authStore from "../../stores/authStore";
import {deleteItem, getBoard, getBoardItems} from "../../api/api";
import { useParams } from 'react-router-dom';
import Loading from "../../components/Loading";


const BoardPage = () => {
    const params = useParams<{boardID: string}>()
    const {data: boardResponse, isLoading: boardLoading} = useQuery(`${authStore.token}-${params.boardID}-board`, getBoard.bind(null, params.boardID))
    const {data: itemsResponse, isLoading: itemsLoading, refetch} = useQuery(`${authStore.token}-${params.boardID}-items`, getBoardItems.bind(null, params.boardID))

    if(itemsLoading || boardLoading)
        return <Loading/>

    return (
        <div className="BoardPage">
            <div className="BoardPage__header">
                <h1>{boardResponse?.data.name}</h1>
                {/*<Tool type={"edit"} size={'large'}/>*/}
                {/*<Tool type={"delete"} size={'large'}/>*/}
            </div>
            {/*itemsResponse?.data||[]*/}
            <ImagesGallery
                onRemove={async (id) => {
                    await deleteItem(boardResponse?.data.id||-1, id)
                    await refetch()
                }}
                images={itemsResponse?.data?.map(({id, image}) => ({id, src: image.src||''}))||[]}
            />
        </div>
    )
};

export default BoardPage;