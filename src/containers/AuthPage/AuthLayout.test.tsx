import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import ReactTestUtils, { act } from "react-dom/test-utils";
import { MemoryRouter } from "react-router-dom";
import AuthLayout from "./AuthLayout";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";
let container: Element | null = null;

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);

    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation(query => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // Deprecated
            removeListener: jest.fn(), // Deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });
});

afterEach(() => {
    if(!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


it("AuthLayout", () => {
    if(!container)
        return

    act(() => {
        render(<AuthLayout>test</AuthLayout>, container);
    });

    expect(container.textContent).toBe("test");
});



let userData = {
    username: 'null',
    password: 'null',
}
let newUser = {}

jest.mock("../../stores/authStore", () => {
    return {
        login: (username: string, password: string) => {
            userData.username = username
            userData.password = password
        },
        register: (vals: any) => {
            newUser=vals
        }
    }
});

it("LoginForm", async () => {
    if(!container)
        return

    act(() => {
        render(<MemoryRouter><LoginForm/></MemoryRouter>, container);
    });

    expect(container.getElementsByClassName('AuthLayout__form_hint')[0].textContent).toBe("Don't have an account yet?  Sign up");

    const inputs = container.getElementsByTagName('input')

    inputs[0].value = 'login'
    ReactTestUtils.Simulate.change(inputs[0]);
    inputs[1].value = 'test'
    ReactTestUtils.Simulate.change(inputs[1]);


    await ReactTestUtils.Simulate.submit(container.getElementsByTagName('form')[0]);

    await sleep(100)
    expect(userData).toStrictEqual({"password": "test", "username": "login"});
});


it("RegisterForm", async () => {
    if(!container)
        return

    act(() => {
        render(<MemoryRouter><RegisterForm/></MemoryRouter>, container);
    });

    expect(container.getElementsByClassName('AuthLayout__form_hint')[0].textContent).toBe("Already have an account?  Sign in");

    const inputs = container.getElementsByTagName('input')

    inputs[0].value = 'username'
    ReactTestUtils.Simulate.change(inputs[0]);
    inputs[1].value = 'email@mail.ru'
    ReactTestUtils.Simulate.change(inputs[1]);
    inputs[2].value = 'pass'
    ReactTestUtils.Simulate.change(inputs[2]);

    inputs[3].value = 'pass1'
    ReactTestUtils.Simulate.change(inputs[3]);


    inputs[3].value = 'pass'
    ReactTestUtils.Simulate.change(inputs[3]);
    inputs[4].value = 'firstName'
    ReactTestUtils.Simulate.change(inputs[4]);
    inputs[5].value = 'lastName'
    ReactTestUtils.Simulate.change(inputs[5]);


    await ReactTestUtils.Simulate.submit(container.getElementsByTagName('form')[0]);

    await sleep(100)

    expect(userData).toStrictEqual({"password": "pass", "username": "username"});
    expect(newUser).toStrictEqual({
        "username": "username",
        "email": "email@mail.ru",
        "first_name": "firstName",
        "last_name": "lastName",
        "password": "pass",
        "password2": "pass",
    });
});