import React from 'react';

type AuthLayoutProps = {
    children: React.ReactNode
}

const AuthLayout = ({children}: AuthLayoutProps) => {

    return (
        <div className="AuthLayout">
            <div className="AuthLayout__content">
                <img src='/logo.svg' alt="logo"/>
                {children}
            </div>
            <div className="AuthLayout__image" style={{backgroundImage: 'url(/login_img.png)'}}/>
        </div>
    )
};

export default AuthLayout;