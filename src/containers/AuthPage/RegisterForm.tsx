import React from 'react';
import {Form, Input, Button} from "antd";
import {Link} from "react-router-dom"
import {LockOutlined, MailOutlined, UserOutlined} from '@ant-design/icons';

import './AuthLayout.css'
import authStore from "../../stores/authStore";

const RegisterForm = () => {
    const onFinish = async (values: RegisterData) => {
        await authStore.register(values)
        await authStore.login(values.username, values.password)
    }

    return (
        <>
            <Form
                name="normal_login"
                className="AuthLayout__form"
                onFinish={onFinish}
            >
                <Form.Item>
                    <h1>Sign up</h1>
                </Form.Item>

                <Form.Item
                    name="username"
                    rules={[{ required: true, message: 'Please input your Username!' }]}
                >
                    <Input
                        style={{background: "transparent", color: "white", padding: 16}}
                        prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                </Form.Item>

                <Form.Item
                    name="email"
                    rules={[{ required: true, message: 'Please input your Email!' }, {type: 'email', message: 'Please input valid Email'}]}
                >
                    <Input
                        autoComplete="new-email"
                        style={{background: "transparent", color: "white", padding: 16}}
                        prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your Password!' }]}
                >
                    <Input.Password
                        autoComplete="off"
                        visibilityToggle={true}
                        style={{background: "transparent", color: "white", padding: 16}}
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item
                    name="password2"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                            },
                        }),
                    ]}
                >
                    <Input.Password
                        autoComplete="off"
                        style={{background: "transparent", color: "white", padding: 16}}
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Confirm Password"
                    />
                </Form.Item>

                <Form.Item
                    name="first_name"
                    rules={[{ required: true, message: 'Please input your First name!' }]}
                >
                    <Input
                        style={{background: "transparent", color: "white", padding: 16}}
                        prefix={<UserOutlined className="site-form-item-icon" />} placeholder="First name" />
                </Form.Item>

                <Form.Item
                    name="last_name"
                    rules={[{ required: true, message: 'Please input your Last name!' }]}
                >
                    <Input
                        autoComplete="new-email"
                        style={{background: "transparent", color: "white", padding: 16}}
                        prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Last name" />
                </Form.Item>


                <Form.Item>
                    <Button type="primary" htmlType="submit" className="AuthLayout__form_button">
                        Sign up
                    </Button>
                    <span className='AuthLayout__form_hint'>Already have an account? &nbsp;<Link to='/login'>Sign in</Link></span>
                </Form.Item>

            </Form>

        </>
    )
};

export default RegisterForm;