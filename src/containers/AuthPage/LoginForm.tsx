import React from 'react';
import {Form, Input, Button} from "antd";
import {Link} from "react-router-dom"
import { LockOutlined, UserOutlined } from '@ant-design/icons';

import './AuthLayout.css'
import authStore from "../../stores/authStore";

const LoginForm = () => {
    const onFinish = ({username, password}: LoginData) => {
        authStore.login(username, password)
    }
    
    return (
        <Form
            name="normal_login"
            className="AuthLayout__form"
            initialValues={{ remember: true }}
            onFinish={onFinish}
        >
            <Form.Item>
                <h1>Sign in</h1>
            </Form.Item>

            <Form.Item
                name="username"
                rules={[{ required: true, message: 'Please input your Username!' }]}
            >
                <Input
                    style={{background: "transparent", color: "white", padding: 16}}
                    prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
            </Form.Item>
            <Form.Item
                name="password"
                rules={[{ required: true, message: 'Please input your Password!' }]}
            >
                <Input
                    style={{background: "transparent", color: "white", padding: 16}}
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                />
            </Form.Item>

            <Form.Item>
                <Button type="primary" htmlType="submit" className="AuthLayout__form_button">
                    Sign in
                </Button>
                <span className='AuthLayout__form_hint'>Don't have an account yet? &nbsp;<Link to='/register'>Sign up</Link></span>
            </Form.Item>
        </Form>
    )
};

export default LoginForm;