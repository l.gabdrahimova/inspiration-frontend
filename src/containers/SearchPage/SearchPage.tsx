import React, {useState} from 'react';
import {AutoComplete, Button, Form, Input, Select} from "antd";
import {SearchOutlined} from '@ant-design/icons';
import InfiniteScroll from 'react-infinite-scroller';


import './SearchPage.css';
import {getBoards, getHistory, search} from "../../api/api";
import ImagesGallery from "../../components/ImagesGallery";
import AddToBoardModal from "./AddToBoardModal";
import {useQuery} from "react-query";
import authStore from "../../stores/authStore";

const {Option} = Select;

const SearchPage = () => {
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState<Image[]>([]);
    const [query, setQuery] = useState<null | {query: string, order_by: string, orientation:string}>(null);
    const [visible, setVisible] = useState(false);
    const [current, setCurrent] = useState<null | {id: string, src: string}>(null);
    const {data: history, isLoading: historyLoading, refetch: refetchHistory} = useQuery(`${authStore.token}-history`, getHistory)

    return (
        <div className="SearchPage">

            {!!query && loading}
            <Form
                className="SearchPage__searchForm"
                onFinish={async ({query, order_by, orientation}) => {
                    setLoading(true)
                    setItems([])
                    setQuery({query, order_by, orientation})
                    // const result = await search(values.query, 0)
                    // setItems(result.data.results.map(r => ({id: r.id, src: r.urls.regular})))
                    setLoading(false)
                }}
            >

                    <Form.Item name={'query'}>
                        <AutoComplete
                            dropdownClassName="certain-category-search-dropdown"
                            dropdownMatchSelectWidth={500}

                            options={history?.data.map(h => ({label: h.query, value: h.query}))}
                        >
                        <Input
                            onClick={() => refetchHistory()}
                            autoComplete={'off'}
                            size="large"
                            placeholder="Search"
                            prefix={<SearchOutlined/>}/>

                        </AutoComplete>
                    </Form.Item>

                <Form.Item>
                    <Button
                        htmlType={"submit"}
                        loading={loading}
                        size="large"
                        type={"primary"}
                        style={{
                            width: '100%',
                            height: 51,
                            padding: 12,
                            borderRadius: 12,
                            fontSize: 'large'
                        }}>Search</Button>
                </Form.Item>

                <Form.Item name={'orientation'} initialValue={""}>
                    <Select defaultValue="" size="large">
                        <Option value="">Any Orientation</Option>
                        <Option value="landscape">Landscape</Option>
                        <Option value="portrait">Portrait</Option>
                        <Option value="squarish">Square</Option>
                    </Select>
                </Form.Item>
                <Form.Item name={'order_by'}  initialValue={"relevant"}>
                    <Select defaultValue="relevant" size="large">
                        <Option value="relevant">Relevance</Option>
                        <Option value="latest">Newest</Option>
                    </Select>
                </Form.Item>
            </Form>

            <AddToBoardModal visible={visible} item={current} onCancel={() => setVisible(false)}/>

            <InfiniteScroll
                pageStart={0}
                loadMore={async (page)=>{
                    if(query){

                        setLoading(true)
                        const result = await search(query.query, query.order_by , page, query.orientation)
                        setItems(prevState => [...prevState, ...result.data.results.map(r => ({id: r.id, src: r.urls.regular}))])

                        setLoading(false)
                    }
                }}
                hasMore={!!query && !loading}
                loader={<div>Loading</div>}>

                <ImagesGallery text={'Input your query and push Search.'} images={items} onAdd={(id, src)=>{
                    setCurrent({id, src})
                    setVisible(true)
                }}/>

            </InfiniteScroll>
            {/*<ImagesGallery images={items} onAdd={(id)=>{*/}
            {/*    setCurrent(id)*/}
            {/*    setVisible(true)*/}
            {/*}}/>*/}

        </div>
    )
};

export default SearchPage;