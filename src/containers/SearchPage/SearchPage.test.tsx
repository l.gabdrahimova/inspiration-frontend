import React from "react";
import {render, unmountComponentAtNode} from "react-dom";
import ReactTestUtils, {act} from "react-dom/test-utils";
import {MemoryRouter} from "react-router-dom";
import {QueryClient, QueryClientProvider} from "react-query";
import SearchPage from "./SearchPage";

let container: Element | null = null;

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);

    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation(query => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // Deprecated
            removeListener: jest.fn(), // Deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });
});

afterEach(() => {
    if (!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


jest.mock("../../stores/authStore", () => ({
    token: '1000'
}));

jest.mock("../../api/api", () => ({
    search: async () => ({
        data: []
    }),
    addToBoard: async () => ({
        data: []
    }),
    createBoards: async () => ({
        data: []
    }),
    getBoards: async () => ({
        data: [{
            id: 10,
            cover: {src: 'https://somelink.com'},
            name: 'Test board',
        }]
    }),
    getRelevantBoards: async () => ({
        data: []
    }),

}));
// jest.mock("antd", () => ({
// // @ts-ignore
//     ...jest.requireActual("antd"),
//     Modal: ({children}) => <div>{children}</div>
// }));


const testQueryClient = new QueryClient()

it("Board page without images", async () => {
    if (!container)
        return

    await act(async () => {
        render(<QueryClientProvider client={testQueryClient}><SearchPage/></QueryClientProvider>, container);


        await sleep(100)
    });

    expect(container.getElementsByClassName('BoardsGallery__noContent')[0].textContent).toBe("Input your query and push Search.");
});