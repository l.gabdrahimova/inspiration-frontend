import React from 'react';

import './MainLayout.css'
import authStore from "../../stores/authStore";
import {Link} from "react-router-dom";

type MainLayoutProps = {
    children: React.ReactNode
}

const MainLayout = ({children}: MainLayoutProps) => {
    return (
        <div className="MainLayout">
            <div className="MainLayout__navbar">
                <img src='/logo.svg' alt="logo"/>

                <Link className="MainLayout__navbar_option" to='/profile'>Profile</Link>
                <Link className="MainLayout__navbar_option" to='/search'>Search</Link>

                <div className="MainLayout__navbar_option logout" style={{marginLeft: 'auto'}} onClick={() => authStore.logout()}>logout</div>
            </div>


            <div className="MainLayout__content">
                {children}
            </div>

        </div>
    )
};

export default MainLayout;