import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import ReactTestUtils, { act } from "react-dom/test-utils";
import MainLayout from "./MainLayout";
import LoginForm from "../AuthPage/LoginForm";
import {MemoryRouter} from "react-router-dom";
let container: Element | null = null;

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);

    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation(query => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // Deprecated
            removeListener: jest.fn(), // Deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });
});

afterEach(() => {
    if(!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

let isLogged = true

jest.mock("../../stores/authStore", () => {
    return {
        logout: () => {isLogged = false}
    }
});

it("AuthLayout", () => {
    if(!container)
        return

    act(() => {
        render(<MemoryRouter><MainLayout>test</MainLayout></MemoryRouter>, container);
    });



    expect(container.getElementsByClassName('MainLayout__content')[0].textContent).toBe("test");

    expect(isLogged).toBe(true);
    ReactTestUtils.Simulate.click(container.getElementsByClassName('MainLayout__navbar_option logout')[0]);
    expect(isLogged).toBe(false);
});