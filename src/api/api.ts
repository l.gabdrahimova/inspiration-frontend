import ax, {AxiosResponse} from "axios";

let baseURL = 'https://enigmatic-scrubland-70991.herokuapp.com/api';

const authApi = ax.create({
    baseURL,
    headers: {
        "Content-Type": "application/json",
    }
});

export const api = ax.create({
    baseURL,
    headers: {
        "Content-Type": "application/json",
    }
});


//Auth
export const login = (username: string, password: string):Promise<AxiosResponse<LoginResponse>> => authApi.post('users/login/', {username, password});
export const register = (data: RegisterData):Promise<AxiosResponse<RegisterResponse>> => authApi.post('users/register/', data);


export const getMe = ():Promise<AxiosResponse<MeResponse>> => api.get('users/me/');
export const uploadAvatar = (file: FormData):Promise<AxiosResponse> => api.post('users/profile/upload_profile_picture/', file, {
    headers: {
        'Content-Type': 'application/json'
    }
});
export const updateProfile = (id: number, data: {first_name: string, last_name: string}):Promise<AxiosResponse> => api.patch(`users/profile/${id}/`, data);


export const getBoardItems = (id: string | number):Promise<AxiosResponse<{id:number, image: {src?: string}}[]>> => api.get(`/boards/${id}/items/`);
export const getBoards = ():Promise<AxiosResponse<Board[]>> => api.get('boards/');
export const getBoard = (id: string):Promise<AxiosResponse<Board>> => api.get(`boards/${id}/`);
export const deleteBoard = (id: string | number):Promise<AxiosResponse> => api.delete(`boards/${id}/`);
export const createBoards = (name: string | number):Promise<AxiosResponse> => api.post('boards/', {name});
export const addToBoard = (board: number, unsplash_id: string, src: string):Promise<AxiosResponse> => api.post(`boards/${board}/items/`, {board, unsplash_id, image:{src}});

export const search = (query: string, order_by: string, page: number, orientation:string):Promise<AxiosResponse<{results: SearchResult[]}>> => api.get(`unsplash/search/`, {params: {query,order_by,orientation, page}});
export const getHistory = ():Promise<AxiosResponse<{query: string}[]>> => api.get(`users/search-history/`);

export const getRelevantBoards = (unsplash_id: string):Promise<AxiosResponse<Board[]>> => api.get(`boards/relevant-boards/`, {params: {unsplash_id}});

export const deleteItem = (board: number, id: string | number):Promise<AxiosResponse> => api.delete(`/boards/${board}/items/${id}/`);