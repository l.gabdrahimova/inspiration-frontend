import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import {MemoryRouter} from "react-router-dom";
import App from "./App";
let container: Element | null = null;

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);

    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation(query => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // Deprecated
            removeListener: jest.fn(), // Deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });
});

afterEach(() => {
    if(!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

jest.mock("./routers/AuthRouter", () => (() => <div>auth</div>));
jest.mock("./routers/AppRouter", () => (() => <div>app</div>));

jest.mock("./stores/authStore", () => {
    return {
        token: 'null'
    }
});

it("App authorized", () => {
    if(!container)
        return

    act(() => {
        render(<MemoryRouter initialEntries={['/login']}><App/></MemoryRouter>, container);
    });


    expect(container.textContent).toBe("app");

});