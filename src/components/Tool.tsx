import React from 'react';
import {FolderAddFilled, DeleteFilled, EditFilled} from '@ant-design/icons';

import './Tool.css'

const icons = {
    add: <FolderAddFilled/>,
    edit: <EditFilled/>,
    delete: <DeleteFilled/>,
}

type ToolProps = {
    type: 'edit' | 'delete' | 'add',
    onClick?: () => any,
    size?: 'medium' | 'large'
}
const Tool = ({type, size = 'medium', onClick}: ToolProps) => {
    return (
        <div
            onClick={onClick}
            className="Tool"
            style={{fontSize: size === 'medium' ? 'medium' : 'x-large', padding: size === 'medium' ? 6 : 6}}
        >
            {icons[type]}
        </div>
    )
};

export default Tool;