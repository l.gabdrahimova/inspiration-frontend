import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Tool from "../Tool";
let container: Element | null = null;


let val = 100

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    if(!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


it("add tool", () => {
    if(!container)
        return

    act(() => {
        render(<Tool type={"add"}/>, container);
    });

    expect(container.getElementsByTagName('span')[0].className).toBe("anticon anticon-folder-add");
});

it("medium edit tool", () => {
    if(!container)
        return

    act(() => {
        render(<Tool type={"edit"}/>, container);
    });

    expect(container.getElementsByTagName('span')[0].className).toBe("anticon anticon-edit");
    expect(container.getElementsByTagName('div')[0].style.fontSize).toBe("medium");
});
it("large delete tool", () => {
    if(!container)
        return

    act(() => {
        render(<Tool type={"delete"} size={"large"}/>, container);
    });

    expect(container.getElementsByTagName('span')[0].className).toBe("anticon anticon-delete");
    expect(container.getElementsByTagName('div')[0].style.fontSize).toBe("x-large");
});

// it("Check onClick", () => {
//     if(!container)
//         return
//
//     ReactTestUtils.Simulate.click(container.getElementsByTagName('img')[0]);
//     expect(val).toBe(0);
// });