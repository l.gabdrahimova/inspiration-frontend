import React from "react";
import {render, unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import ReactTestUtils from 'react-dom/test-utils';
import Photo from "../Photo";

let container: Element | null = null;


let action = 'none'

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);

    act(() => {
        render(<Photo
            onRemove={(id) => {
                action = `remove ${id}`
            }}
            onAdd={(id) => {
                action = `add ${id}`
            }}
            id={'1'}
            src={'https://somelink.com/'}

        />, container);
    });
});

afterEach(() => {
    if (!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


it("Check link and alt", () => {
    if (!container)
        return

    expect(container.getElementsByTagName('img')[0].src).toBe("https://somelink.com/");
    expect(container.getElementsByTagName('img')[0].alt).toBe("img");
});

it("Check tools", () => {
    if (!container)
        return

    expect(container.getElementsByClassName('Tool').length).toBe(2);
});


it("Check onRemove", () => {
    if (!container)
        return

    ReactTestUtils.Simulate.click(container.getElementsByClassName('Tool')[0]);
    expect(action).toBe(`add 1`);
});

it("Check onAdd", () => {
    if (!container)
        return

    ReactTestUtils.Simulate.click(container.getElementsByClassName('Tool')[1]);
    expect(action).toBe(`remove 1`);
});

it("Check no tools", () => {
    if (!container)
        return

    act(() => {
        render(<Photo
            id={'1'}
            src={'https://somelink.com/'}

        />, container);
    });


    expect(container.getElementsByClassName('Tool').length).toBe(0);
});

