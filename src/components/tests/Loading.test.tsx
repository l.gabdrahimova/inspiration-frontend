import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import Loading from "../Loading";
let container: Element | null = null;
function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    if(!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});


it("check progress", async () => {

    if(!container)
        return

    await act(async () => {
        render(<Loading />, container);

    });

    // @ts-ignore
    expect(container.getElementsByClassName('Loading__bar_progress')[0].style.width).toBe('0%');
    await act(async () => {
        if(!container)
            return
        await sleep(100)
        // @ts-ignore
        expect(container.getElementsByClassName('Loading__bar_progress')[0].style.width).toBe(`${1+(100)/50}%`);
    });

});

