import React, {useEffect} from 'react';

import './BoardsGallery.css'
import Photo from "./Photo";

type ImagesGalleryProps = {
    images: Image[]
    onAdd?: (id: string, src: string) => any
    onRemove?: (id: string) => any
    text?: string
}

const ImagesGallery = ({images, onAdd, onRemove, text}: ImagesGalleryProps) => {

    useEffect(() => {
        // window.scrollTo(0, 0);
    }, [])


    return (images.length
            ? <div className="BoardsGallery">
                {
                    images.map(image => (
                        <div className="BoardsGallery__item" key={image.id.toString()}>
                            <Photo
                                src={image.src}
                                id={image.id.toString()}
                                onAdd={onAdd}
                                onRemove={onRemove}
                            />
                        </div>))
                }
            </div>
            : <div className="BoardsGallery__noContent">
                <img src={'/no_collection_1.png'} alt={'no collection'}/>

                {text||<>
                    You don't have any photos in this collection<br/>
                    so there's nothing to display here. Sad panda.<br/><br/>

                    Add a photo now!
                </>}




            </div>
    )
};

export default ImagesGallery;