import React from 'react';

import './BoardsGallery.css'
import Board from "./Board";

type BoardsGalleryProps = {
    boards: Board[],
    onChoose?: (id: number) => void
    onRemove?: (id: number) => void
}

const BoardsGallery = ({boards, onChoose, onRemove}: BoardsGalleryProps) => {

    return boards.length
        ? <div className="BoardsGallery">
            {boards.map(board => (
                <div className="BoardsGallery__item" key={board.id}>
                    <Board board={board} key={board.id} onClick={onChoose} onRemove={onRemove}/>
                </div>
            ))}
        </div>
        : <div className="BoardsGallery__noContent" style={{height: "auto"}}>
            <img src={'/no_collection_1.png'} alt={'no collection'}/>

            You don't have any collections<br/>
            so there's nothing to display here. Sad panda.<br/><br/>

            Add a photo now!


        </div>
};

export default BoardsGallery;