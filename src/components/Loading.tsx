import React, {useEffect, useState} from 'react';

import './Loading.css'

const Loading = () => {
    const [progress, setProgress] = useState(0)

    useEffect(()=>{
        if(progress < 100){

            const t = setTimeout(() => setProgress(prevState => prevState + 1+(100-prevState)/50), 100)

            return () => {
                clearTimeout(t)
            }
        }

    }, [progress])

    return (
        <div className="Loading">

            <img src={'/loading.png'} alt={'loading'}/>

            <div className="Loading__bar">
                <div className="Loading__bar_progress" style={{width: `${progress}%`}}/>
            </div>


            <h1>Loading</h1>
        </div>
    )
};

export default Loading;