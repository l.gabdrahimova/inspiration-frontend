import ReactTestUtils, {act, Simulate} from "react-dom/test-utils";
import authStore from "./authStore";

const fakeApi = {
    login: async (u: string, p:string) => {
        return Promise.reject()
    },
    register:  async () => {
        return Promise.reject()
    },

    api:{
        interceptors:{
            request:{
                use: async () => {},
            },
            response:{
                use: async () => {},
            }
        }
    }
}

jest.mock("../api/api", () => {
    return fakeApi
});


it("login with error", async () => {

    expect(authStore.token).toBe(null);
    await authStore.login('test', 'pass')
    expect(authStore.token).toBe(null);
    await authStore.logout()
    expect(authStore.token).toBe(null);
    try {
        await authStore.register({email:'', username:'', password:'', password2:''})
    }catch (e){}
    expect(authStore.token).toBe(null);

});