import {action, makeAutoObservable, observable} from "mobx";
import {notification} from "antd";
import Cookie from "js-cookie"
import {AxiosRequestConfig} from "axios";

import * as api from "../api/api";

class AuthStore {
    @observable
    token: string | null

    constructor() {
        makeAutoObservable(this)
        const token = Cookie.get('token')
        this.token = token || null
    }

    @action
    async login(username: string, password: string) {
        try {
            const resp = await api.login(username, password)
            const data = resp.data
            this.token = data.token || null

            Cookie.set("token", this.token || "");
        } catch (e) {
            notification.error({message: e?.response?.data?.non_field_errors?.join('\n') || 'Unknown error'})
            // notification.error({message: JSON.stringify(e.response)})
            console.log(e)
        }
    }

    @action
    async register(registerData: RegisterData) {
        try {
            await api.register(registerData)
            // const data = resp.data
            // this.token = data.token || null
            //
            // Cookie.set("token", this.token || "");
        } catch (e) {
            const errors = Object.values(e?.response?.data || {})
            if(errors.length)
                errors.forEach(error => notification.error({message: `${error}`}))
            else
                notification.error({message: 'Unknown error'})
            console.log(e)
            return Promise.reject(e);
        }
    }

    @action
    async logout(){
        this.token = null;

        Cookie.remove('token')
    }

}

const authStore = new AuthStore();

api.api.interceptors.request.use((config: AxiosRequestConfig) => {
    config.headers[ "Authorization"] = `Token ${authStore.token}`;
    return config;
});

api.api.interceptors.response.use(undefined, async (error: any) => {
    if (error.response && error.response.status === 401) {
        await authStore.logout()
    }
    return Promise.reject(error);
});



export default authStore