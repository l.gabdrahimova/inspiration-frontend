import ReactTestUtils, {act, Simulate} from "react-dom/test-utils";

import authStore from "./authStore";
let f1: ((c: any) => void)|null = null



let f2: ((c: any) => void)|null = null

const fakeApi = {
    login: async (u: string, p:string) => {
        // return Promise.reject()

        return {
            data: {
                token: u+p
            }
        }
    },
    register:  async () => {},

    api:{
        interceptors:{
            request:{
                use: (f: any) => {
                    let config: any = {headers: {}}
                    config = f&&f(config)
                    expect(config).toStrictEqual({headers: {"Authorization": "Token null"}});
                },
            },
            response:{
                use: async (c:any, f: any) => {
                    await f({response: {status: 401}})
                },
            }
        }
    }
}

jest.mock("../api/api", () => {
    return fakeApi
});

it("Login", async () => {


    expect(authStore.token).toBe(null);
    await authStore.login('test', 'pass')
    expect(authStore.token).toBe('testpass');
    await authStore.logout()
    expect(authStore.token).toBe(null);
    await authStore.register({email:'', username:'', password:'', password2:''})
    expect(authStore.token).toBe(null);

});
