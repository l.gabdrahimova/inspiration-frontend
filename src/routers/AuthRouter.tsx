import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import LoginForm from "../containers/AuthPage/LoginForm";
import RegisterForm from "../containers/AuthPage/RegisterForm";
import AuthLayout from "../containers/AuthPage/AuthLayout";


const AuthRouter = () => {
    return (
        <Router>
            <AuthLayout>
                <Switch>
                    <Route path='/login' exact><LoginForm/></Route>
                    <Route path='/register' exact><RegisterForm/></Route>
                </Switch>
            </AuthLayout>
        </Router>
    )
};

export default AuthRouter;