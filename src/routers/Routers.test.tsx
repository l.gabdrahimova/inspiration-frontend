import React from "react";
import {render, unmountComponentAtNode} from "react-dom";
import {act} from "react-dom/test-utils";
import AppRouter from "./AppRouter";
import AuthRouter from "./AuthRouter";

let container: Element | null = null;


beforeEach(() => {
    // подготавливаем DOM-элемент, куда будем рендерить
    container = document.createElement("div");
    document.body.appendChild(container);

    Object.defineProperty(window, 'matchMedia', {
        writable: true,
        value: jest.fn().mockImplementation(query => ({
            matches: false,
            media: query,
            onchange: null,
            addListener: jest.fn(), // Deprecated
            removeListener: jest.fn(), // Deprecated
            addEventListener: jest.fn(),
            removeEventListener: jest.fn(),
            dispatchEvent: jest.fn(),
        })),
    });
});

afterEach(() => {
    if (!container)
        return
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("AppRouter", () => {
    if (!container)
        return

    act(() => {
        render(
            <AppRouter/>, container);
    });

    expect(container.textContent).toBe("ProfileSearchlogout");
});
it("AuthRouter", () => {
    if (!container)
        return

    act(() => {
        render(
            <AuthRouter/>, container);
    });

    expect(container.textContent).toBe("");
});