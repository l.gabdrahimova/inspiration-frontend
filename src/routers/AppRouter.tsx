import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import MainLayout from "../containers/Layout/MainLayout";
import ProfilePage from "../containers/ProfilePage/ProfilePage";
import BoardPage from "../containers/BoardPage/BoardPage";
import SearchPage from "../containers/SearchPage/SearchPage";

const AppRouter = () => {

    return (
        <Router>
            <MainLayout>
                <Switch>
                    <Route path='/profile' exact><ProfilePage/></Route>
                    <Route path='/profile/:boardID' exact><BoardPage/></Route>
                    <Route path='/search' exact><SearchPage/></Route>
                </Switch>
            </MainLayout>
        </Router>
    )
};

export default AppRouter;