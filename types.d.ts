type LoginData = {
    username: string
    password: string
}

type LoginResponse = {
    username: string
    password: string
    token: string
}


type RegisterData = {
    email: string
    username: string
    password: string
    password2: string
}

type RegisterResponse = {
    username: string
    password: string
    token: string
}

type MeResponse = {
    id: number
    username: string
    email: string
    first_name: string
    last_name: string
    profile?: {
        pic: string
    }
}
type Board = {
    id: number
    cover: {src: string} | null
    name: string
}

type Image = {
    id: number | string
    src: string
}

type SearchResult = {
    id: string
    urls: {
        small: string,
        regular: string
    }
}